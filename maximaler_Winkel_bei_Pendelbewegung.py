import numpy as np

####################################################
position=np.array([[13.0,14.0,15.0],[13.0,14.0,15.0],[13.0,14.0,15.0],[14.0,14.1,15.1],[15.0,14.5,16.0],[16.0,15.0,17.0],[17.0,14.5,20.0],[16.9,15.4,21.0]])

####################################################
def get_angel(position,arm_length):
    '''
    Die Methode berechnet den maximalen Winkel eines pendelnden Arms, der seitlich am Körper vorbei gezogen wird. (Von der Hüfte zum höchsten Punkt)
    WICHTIG:
    Die ersten 3 Sekunden den Arm Hängen lassen (starten auch mit hängendem Arm)
    
    Args
    :param position: 3d Position np.array([x,y,z])
    :param arm_length: float --Von der Hand bis zur Schulter

    :return: Winkel° 
    '''
    fz=4 #Mittelwert der ersten 3 sekunden...abhängig von Frequenz...hier nur die ersten 4 Werte
    mean_of_the_first_values=np.mean(position[:fz, :], axis=0)
    norm_pos=[]
    for pos in position:
        norm_pos.append([pos[0]-mean_of_the_first_values[0],pos[1]-mean_of_the_first_values[1],pos[2]-mean_of_the_first_values[2]])
    pos=np.array([norm_pos])
    max=0
    idx=0
    i=0
    for p in norm_pos:
        if(p[2]>max):
            max=p[2]
            idx=i
        i=i+1
    dist=np.sqrt(norm_pos[idx][0]**2+norm_pos[idx][1]**2+norm_pos[idx][2]**2)
   # print(dist)
    angle=(180.0-90-np.degrees(np.arccos((dist/2)/arm_length)))*2
    return angle

print(get_angel(p1,5.0))
