import Plot from "react-plotly.js";
// Or using require,
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { execute } from "./execute";

const samplePos = () => {
  const cord = [];
  const rawData = [];
  for (let index = 0; index < 200; index++) {
    cord.push({ x: index, y: index, z: index });
    cord.push({
      acc: { x: index, y: index, z: index },
      gyro: { x: index, y: index, z: index }
    });
  }
  return {
    cords: cord,
    rawData: rawData,
    angleAnalysis: {
      filteredPoints: cord,
      angle: { pointA: { x: 1, y: 1 }, pointB: { x: 0, y: 1 }, angle: 90 },
      circle: {
        x: 0,
        y: 0,
        r: 0
      }
    }
  };
};

function App() {
  const [movement, setMovement] = useState(samplePos());
  const [id, setId] = useState("");

  const x = movement.cords.map(pos => pos.x);
  const y = movement.cords.map(pos => pos.y);
  const z = movement.cords.map(pos => pos.z);
  let max = 0;
  let min = 0;

  x.forEach(item => {
    if (item < min) min = item;
    if (item > max) max = item;
  });
  y.forEach(item => {
    if (item < min) min = item;
    if (item > max) max = item;
  });
  z.forEach(item => {
    if (item < min) min = item;
    if (item > max) max = item;
  });
  const accx = {
    x: movement.rawData?.map((pos, index) => index),
    y: movement.rawData?.map(pos => pos.acc.x)
  };
  const accy = {
    x: movement.rawData?.map((pos, index) => index),
    y: movement.rawData?.map(pos => pos.acc.y)
  };
  const accz = {
    x: movement.rawData?.map((pos, index) => index),
    y: movement.rawData?.map(pos => pos.acc.z)
  };

  const gyrox = {
    x: movement.rawData?.map((pos, index) => index),
    y: movement.rawData?.map(pos => pos.gyro.x)
  };
  const gyroy = {
    x: movement.rawData?.map((pos, index) => index),
    y: movement.rawData?.map(pos => pos.gyro.y)
  };
  const gyroz = {
    x: movement.rawData?.map((pos, index) => index),
    y: movement.rawData?.map(pos => pos.gyro.z)
  };

  const filter = {
    x: movement.angleAnalysis?.filteredPoints?.map(pos => pos.x),
    y: movement.angleAnalysis?.filteredPoints?.map(pos => pos.y)
  };
  const filterAnglePoints = {
    x: [
      movement.angleAnalysis?.angle.pointA.x,
      movement.angleAnalysis?.circle.x,
      movement.angleAnalysis?.angle.pointB.x
    ],
    y: [
      movement.angleAnalysis?.angle.pointA.y,
      movement.angleAnalysis?.circle.y,
      movement.angleAnalysis?.angle.pointB.y
    ]
  };
  let filterMin = 0;
  let filterMax = 0;
  filter.x.forEach((item, i) => {
    if (item < filterMin) filterMin = item;
    if (item > filterMax) filterMax = item;
  });
  filter.y.forEach((item, i) => {
    if (item < filterMin) filterMin = item;
    if (item > filterMax) filterMax = item;
  });
  return (
    <div className="App">
      <input type="text" value={id} onChange={e => setId(e.target.value)} />
      <button
        onClick={() =>
          execute("/movement/3dPostions?id=" + id)
            .then(res => {
              if (res) setMovement(res);
            })
            .catch(err =>
              toast.error("Die angeforderte Übung konnte nicht geladen werden!")
            )
        }
      >
        Get Data
      </button>
      <div id="graphs">
        <div className="processed">
          <Plot
            data={[
              {
                x: x,
                y: y,
                z: z,
                type: "scatter3d",
                mode: "lines",
                marker: { color: "red" }
              }
            ]}
            layout={{
              width: 600,
              height: 450,
              scene: {
                xaxis: {
                  range: [min, max],
                  autorange: false,
                  rangemode: "tozero",
                  fixedrange: true
                },
                yaxis: {
                  range: [min, max],
                  autorange: false,
                  rangemode: "tozero",
                  fixedrange: true
                },
                zaxis: {
                  range: [min, max],
                  autorange: false,
                  rangemode: "tozero",
                  fixedrange: true
                }
              },
              title: "Movement Visualisation"
            }}
          />
          <Plot
            data={[
              {
                ...filter,
                type: "scatter",
                mode: "markers",
                marker: { color: "red" },
                name: "X"
              },
              {
                ...filterAnglePoints,
                type: "scatter",
                mode: "line",
                marker: { color: "blue" },
                name: "Angle"
              }
            ]}
            layout={{
              width: 600,
              height: 450,
              scene: {
                xaxis: {
                  range: [filterMin, filterMax],
                  scaleanchor: "y",
                  scaleratio: 1
                },
                yaxis: {
                  range: [filterMin, filterMax],
                  scaleanchor: "x",
                  scaleratio: 1
                }
              },
              title: "Analysis",
              annotations: [
                {
                  x: movement.angleAnalysis.circle.x,
                  y:
                    movement.angleAnalysis.circle.y +
                    (filterMax - filterMin) / 20,
                  xref: "x",
                  yref: "y",
                  text: String(
                    Math.round(movement.angleAnalysis.angle.angle) + "°"
                  ),
                  showarrow: false,
                  ax: 0
                }
              ],
              shapes: [
                {
                  type: "circle",
                  fillcolor: "rgba(50, 171, 96, 0.7)",
                  x0:
                    movement.angleAnalysis.circle.x -
                    movement.angleAnalysis.circle.r,
                  x1:
                    movement.angleAnalysis.circle.x +
                    movement.angleAnalysis.circle.r,
                  y0:
                    movement.angleAnalysis.circle.y -
                    movement.angleAnalysis.circle.r,
                  y1:
                    movement.angleAnalysis.circle.y +
                    movement.angleAnalysis.circle.r
                }
              ]
            }}
          />
        </div>

        <Plot
          data={[
            {
              ...accx,
              type: "scatter",
              mode: "lines",
              marker: { color: "red" },
              name: "X"
            },
            {
              ...accy,
              type: "scatter",
              mode: "lines",
              marker: { color: "green" },
              name: "Y"
            },
            {
              ...accz,
              type: "scatter",
              mode: "lines",
              marker: { color: "blue" },
              name: "Z"
            }
          ]}
          layout={{
            width: 600,
            height: 900,
            scene: {
              xaxis: {
                range: [min, max],
                autorange: false,
                rangemode: "tozero",
                fixedrange: true
              },
              yaxis: {
                range: [min, max],
                autorange: false,
                rangemode: "tozero",
                fixedrange: true
              },
              zaxis: {
                range: [min, max],
                autorange: false,
                rangemode: "tozero",
                fixedrange: true
              }
            },
            title: "Acc Visual"
          }}
        />
        <Plot
          data={[
            {
              ...gyrox,
              type: "scatter",
              mode: "lines",
              marker: { color: "red" },
              name: "X"
            },
            {
              ...gyroy,
              type: "scatter",
              mode: "lines",
              marker: { color: "green" },
              name: "Y"
            },
            {
              ...gyroz,
              type: "scatter",
              mode: "lines",
              marker: { color: "blue" },
              name: "Z"
            }
          ]}
          layout={{
            width: 600,
            height: 900,
            scene: {
              xaxis: {
                range: [min, max],
                autorange: false,
                rangemode: "tozero",
                fixedrange: true
              },
              yaxis: {
                range: [min, max],
                autorange: false,
                rangemode: "tozero",
                fixedrange: true
              },
              zaxis: {
                range: [min, max],
                autorange: false,
                rangemode: "tozero",
                fixedrange: true
              }
            },
            title: "Gryo Visual"
          }}
        />
      </div>
    </div>
  );
}

export default App;
