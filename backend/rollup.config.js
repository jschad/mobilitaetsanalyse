// rollup.config.js
import resolveLocal from "rollup-plugin-local-resolve";
import json from "rollup-plugin-json";

export default {
  input: "build/server.js",
  output: {
    file: "build/bundle.js",
    format: "cjs"
  },
  plugins: [json(), resolveLocal()]
};
