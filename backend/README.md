# mobilitaetsanalyse node.js backend

The RESTful Node.js server backend for an Android fitness tracker.

## Dependencies

- Node.js
- npm
- MongoDB installation (command `mongod` has to work in console)

## Installing

- Clone the project.
- Run `npm install`.

## Executing program

- `npm run db` starts the database (linus & macOS only).
- `npm run win-db` starts the database on windows.
- `npm run dev` starts the server for development (without hot-reloading).
- `npm run watch` starts the server for development with hot-reloading.
- `npm run build` creates a single production.js file in the `/build` folder for deployment.
- `npm start` starts the production server.

## Production server

### Node app in production

When merging into or commiting to the dev branch the CI/CD pipeline is triggered and deploys the node application to our Heroku production environment.

The already SSL encrypted production url is https://sharelog-backend.herokuapp.com/.

To make API requests, simply append the desired route, i.e. `...herokuapp.com/user`. The application switches into sleep mode if there were no API calls in the last 30 minutes. For that reason it might take 5-10 seconds for the API to respond on the first call.

Under https://sharelog-backend.herokuapp.com/status you can find a monitoring screen for the app.

### MongoDB database in production

The production database we use is hosted on MongoDB Atlas. It is running 24/7. You can connect to it by copying

`mongodb+srv://dbUser:%23CdbzP5o%2653n@cluster0-qjejq.mongodb.net/sharelogdb`

to your clipboard and running MongoDB Compass which will automatically recognize the clipboard content as a connection string.
