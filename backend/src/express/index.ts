/**
 * This Module sets up the REST-API and CORS-Handling
 */

/** */
import bodyParser from "body-parser";
import express from "express";

import movementsRouter from "./routes/movement/movement";
import routeLogging from "./logging/RequestLogger";

/**
 * Sets up all REST-API functions and CORS-Handling
 * @param app instance of the used express application
 */
const setupExpress = (app: express.Application) => {
  app.use(routeLogging);
  app.use(require("express-status-monitor")());
  app.use(bodyParser.json({ limit: "50mb" }));
  app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
  app.use((req: express.Request, res: express.Response, next: () => void) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Header-Custom-Tizen");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");

    next();
  });

  app.use("/movement", movementsRouter);
};

export default setupExpress;
