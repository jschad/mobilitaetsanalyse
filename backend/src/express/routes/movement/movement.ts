import express from "express";

import Movement from "../../../mongodb/schemes/dao/movement";
import { IMovement, MovementTypes, RawDataPoint, Cords, AngleAnalysis } from "../../../mongodb/schemes/models";
import fetch from "node-fetch";
import https from "https";
/**
 * An Express Route for all Endpoints related to User Requests
 */

const MovementRouter = express.Router();

/**
 * Defines the GET route 'movements/'
 * returns a movement with a given id
 */

MovementRouter.get("/test", async (req: express.Request, res: express.Response) => {
  res.json({ success: true });
});

MovementRouter.post("/create", async (req: express.Request, res: express.Response) => {
  try {
    const movementType = req.body.type as MovementTypes;
    const raw = req.body.data as RawDataPoint[];
    const httpsAgent = new https.Agent({
      rejectUnauthorized: false
    });
    const gyr: number[] = [];
    const acc: number[] = [];

    raw.forEach((item, index) => {
      if (item.gyro && item.acc) {
        gyr.push(item.gyro?.x);
        gyr.push(item.gyro?.y);
        gyr.push(item.gyro?.z);
        acc.push(item.acc?.x);
        acc.push(item.acc?.y);
        acc.push(item.acc?.z);
      } else {
        console.log(item, index);
      }
    });
    const response = await fetch("https://localhost:4000/analysis", {
      method: "post",
      body: JSON.stringify({ gyr, acc }),
      headers: { "Content-Type": "application/json" },
      agent: httpsAgent
    });

    const responseJson: { x: number[]; y: number[]; z: number[] } = await response.json();
    const cords: Cords[] = [];
    responseJson.x.forEach((x, index) => {
      cords.push({
        id: index,
        x,
        y: responseJson.y[index],
        z: responseJson.z[index],
        timestamp: raw[index]?.timestamp
      });
    });

    /*const circleCords = [];
    const r = 50;
    for (let i = 0; i < 180; i++) {
      const x = (r * i) / 90 - r;
      circleCords.push({
        x: x,
        y: Math.sqrt(Math.pow(r, 2) - Math.pow(x, 2))
      });
    }*/

    const responseCircle = await fetch("https://localhost:4000/circle", {
      method: "post",
      body: JSON.stringify(cords),
      headers: { "Content-Type": "application/json" },
      agent: httpsAgent
    });
    const cricleJson: AngleAnalysis = await responseCircle.json();

    let movement: IMovement = {
      type: movementType,
      date: new Date().toISOString(),
      cords: cords,
      rawData: raw,
      angleAnalysis: cricleJson
    };
    const movementRes = await Movement.insertMovement(movement);
    console.log(movementRes?._id);

    res.json(movementRes);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false });
  }
});
MovementRouter.get("/3dPostions", async (req: express.Request, res: express.Response) => {
  try {
    const movementId = req.query.id as MovementTypes;
    const movement = await Movement.getPostions(movementId);

    res.json(movement);
  } catch (error) {
    res.status(500).json({ success: false });
  }
});

export default MovementRouter;
