import express from "express";
/**
 * logs all incoming routes to console
 *
 * @param req Request provided by express
 * @param res Response provided by express
 * @param next callback function
 */
const routeLogging = (req: express.Request, res: express.Response, next: () => void) => {
  //console.log(req.headers);

  const msg = "New " + req.method + ' request to route "' + req.originalUrl + '".';
  console.log(msg);
  next();
};

export default routeLogging;
