import express from "express";
import * as http from "http";
import dotenv from "dotenv";

import setupExpress from "./express";
import setupMongoDB from "./mongodb";
import initializeExampleData from "./mongodb/init";

dotenv.config();

const app = express();
const server = http.createServer(app);

setupExpress(app);
setupMongoDB();
initializeExampleData();

server.listen(process.env.PORT, () =>
  console.log(`mobilitaetsanalyse-Backend-Server is running on port ${process.env.PORT}.`)
);
