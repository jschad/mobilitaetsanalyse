import mongoose, { Document, Schema } from "mongoose";

import { Converter } from "../../utils";

export enum MovementTypes {
  Jogging = "Walk",
  Cycling = "Ride",
  None = "none"
}
export interface Cords {
  id: number;
  timestamp: number;
  x: number;
  y: number;
  z: number;
}
export interface RawDataPoint {
  id: number;
  timestamp: number;
  acc: {
    x: number;
    y: number;
    z: number;
  };
  gyro: {
    x: number;
    y: number;
    z: number;
  };
}
export interface AngleAnalysis {
  circle: {
    x: number;
    y: number;
    r: number;
  };
  filteredPoints: {
    x: number;
    y: number;
  }[];
  angle: {
    angle: number;
    pointA: {
      x: number;
      y: number;
    };
    pointB: {
      x: number;
      y: number;
    };
  };
}
export interface IMovement {
  _id?: any;
  type: MovementTypes;
  date: string;
  rawData: RawDataPoint[];
  angleAnalysis: AngleAnalysis;

  cords: Cords[];
}
interface IMovementSchema extends Document, IMovement {
  _id: any; //This propably could also be Schema.Types.ObjectId | string but the default in mongoose is any
}
/**
 * Interface defines the Challenge document type
 */
const MovementSchema = new Schema({
  type: {
    required: true,
    type: String,
    enum: Converter.convertEnumToMongooseArray(MovementTypes),
    default: MovementTypes.None
  },

  date: { required: true, type: String, default: "0" },
  rawData: [
    {
      id: { required: true, type: Number, default: 0 },
      timestamp: { required: true, type: Number, default: 0 },
      acc: {
        x: { required: true, type: Number, default: 0 },
        y: { required: true, type: Number, default: 0 },
        z: { required: true, type: Number, default: 0 }
      },

      gyro: {
        x: { required: true, type: Number, default: 0 },
        y: { required: true, type: Number, default: 0 },
        z: { required: true, type: Number, default: 0 }
      }
    }
  ],
  cords: [
    {
      id: { required: true, type: Number, default: 0 },
      timestamp: { required: true, type: Number, default: 0 },
      x: { required: true, type: Number, default: 0 },
      y: { required: true, type: Number, default: 0 },
      z: { required: true, type: Number, default: 0 }
    }
  ],
  angleAnalysis: {
    circle: {
      x: { required: true, type: Number, default: 0 },
      y: { required: true, type: Number, default: 0 },
      r: { required: true, type: Number, default: 0 }
    },
    filteredPoints: [
      {
        x: { required: true, type: Number, default: 0 },
        y: { required: true, type: Number, default: 0 }
      }
    ],
    angle: {
      angle: { required: true, type: Number, default: 0 },
      pointA: {
        x: { required: true, type: Number, default: 0 },
        y: { required: true, type: Number, default: 0 }
      },
      pointB: {
        x: { required: true, type: Number, default: 0 },
        y: { required: true, type: Number, default: 0 }
      }
    }
  }
});
export default mongoose.model<IMovementSchema>("Movement", MovementSchema);
