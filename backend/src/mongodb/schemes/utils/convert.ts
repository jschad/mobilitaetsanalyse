/**
 * converts an Typscript Enum to an Mongoose Enum
 * @param enumObject any given Typescript Enum
 */

export const convertEnumToMongooseArray = (enumObject: any): Array<any> => {
  const arr: Array<any> = [];
  Object.keys(enumObject).forEach((key: string) => arr.push(enumObject[key]));
  return arr;
};
export const getCurrentDateAsString = (): string => {
  return convertDateToString(new Date());
};
export const convertDateToString = (date: Date): string => {
  // this converts a given Date to the following format: YYYY.MM.DD-hh:mm:ss
  const year: number = date.getFullYear();
  const month: number = date.getMonth();
  const day: number = date.getDate();
  const hour: number = date.getHours();
  const minute: number = date.getMinutes();
  const second: number = date.getSeconds();

  return day + "." + month + "." + year + " " + hour + ":" + minute + ":" + second;
};
export const convertStringToDate = (dateStr: String): Date => {
  // this converts a given Date to the following format: YYYY.MM.DD hh:mm:ss
  const dayStr = dateStr.split(" ")[0];
  const timeStr = dateStr.split(" ")[1];
  const dateArr = dayStr
    .split(".")
    .concat(timeStr.split(":"))
    .map(elem => parseInt(elem, 10));

  return new Date(dateArr[2], dateArr[1], dateArr[0], dateArr[3], dateArr[4], dateArr[5]);
};
