import movements, { RawDataPoint, IMovement, Cords } from "../models/movement/movement";
/**
 * finds a movement to a given is
 * @param movementId
 * @returns a movement
 */
const getMovement = (movementId: string) => {
  return movements.findOne({ _id: movementId });
};
/**
 * inserts a new movement
 * @param movement
 */
const insertMovement = (movement: IMovement) => {
  return movements.create(movement).then(doc => {
    return getMovement(doc._id);
  });
};

const updateRawData = (movementId: string, update: RawDataPoint[]) => {
  return movements.findOne({ _id: movementId }).then((doc: any) => {
    if (doc) {
      doc.rawData = update;

      doc.save();
      return { success: true };
    } else {
      return { success: false };
    }
  });
};
const updateCords = (movementId: string, update: Cords[]) => {
  return movements.findOne({ _id: movementId }).then((doc: any) => {
    if (doc) {
      doc.cords = update;

      doc.save();
      return { success: true };
    } else {
      return { success: false };
    }
  });
};
const getPostions = (movementId: string) => {
  return movements.findOne({ _id: movementId });
};

export default {
  getMovement,
  updateRawData,
  insertMovement,
  updateCords,
  getPostions
};
