import mongoose from "mongoose";

/**
 * Sets up mongodb connection
 */
const setupMongoDB = () => {
  const mongoUrl = process.env.MONGO_URL as string;

  mongoose.set("useCreateIndex", true);
  mongoose
    .connect(mongoUrl, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
      useUnifiedTopology: true
    })
    .catch(err => {
      console.error("MongoDB connection error. Have you started you Database?");
      console.error("Retrying again in 2 Seconds");
      console.error(err);
      window.setTimeout(() => setupMongoDB(), 2000);
    });

  const db = mongoose.connection;

  db.on("connected", console.log.bind(console, "Connected to MongoDB!"));
  db.on("error", console.error.bind(console, "MongoDB connection error:"));
};

export default setupMongoDB;
