var fetchNode = require("node-fetch");
const https = require("https");
const csv = require("csvtojson");

async function doStuff() {
  let acc = null;
  try {
    acc = await csv().fromFile(process.argv[2] + "/Accelerometer.csv");
  } catch (error) {
    acc = await csv().fromFile(process.argv[2] + "/AccelerometerLinear.csv");
  }

  const gyr = await csv().fromFile(process.argv[2] + "/Gyroscope.csv");

  const combine = [];

  for (let index = 0; index < (acc.length < gyr.length ? acc.length : gyr.length); index++) {
    const date = new Date(acc[index].Timestamp);
    date.setMilliseconds(parseInt(acc[index].Milliseconds));
    if (index % 1 === 0) {
      combine.push({
        timestamp: date.getTime(),
        acc: { x: parseFloat(acc[index].X), y: parseFloat(acc[index].Y), z: parseFloat(acc[index].Z) },
        gyro: {
          x: parseFloat(gyr[index].X) * (180 / Math.PI),
          y: parseFloat(gyr[index].Y) * (180 / Math.PI),
          z: parseFloat(gyr[index].Z) * (180 / Math.PI)
        }
      });
    }
  }

  const response = await fetchNode("http://localhost:3002/movement/create", {
    method: "post",
    body: JSON.stringify({ type: "Walk", data: combine }),
    headers: { "Content-Type": "application/json" }
  });
  console.log("pushed ", combine.length, " datapoints");
}
doStuff();
