#!/bin/bash
# Check if we already have a docker container with the correct name
CONTAINER_NAME="mobilitaet-db"
if docker container ls -a | grep "$CONTAINER_NAME" >/dev/null; then
    if docker ps | grep "$CONTAINER_NAME" > /dev/null; then
        echo "Container with name $CONTAINER_NAME is already running, doing nothing."
        exit 1
    fi
    echo "Starting existing container with name $CONTAINER_NAME"
    docker start "$CONTAINER_NAME"
else
    user="privacymail"
    password="privacymail-pass"
    if [ "$user" = "" ] || [ "$password" = "" ]; then
        echo "Please set the privacymail/postgres-user and privacymail/postgres-password keys in pass. See documentation for details."
        unset user
        unset password
        exit 1
    fi
    echo "Spinning up new development database container with name $CONTAINER_NAME."
    docker run --name "$CONTAINER_NAME"  -p 27017:27017 -d mongo:latest #-e MONGO_INITDB_ROOT_USERNAME=$user -e MONGO_INITDB_ROOT_USERNAME=$password
    unset user
    unset password
fi
