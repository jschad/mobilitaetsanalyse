import copy
from flask import Flask, escape, request, abort, jsonify
from __main__ import app
from scipy.optimize import minimize,leastsq
import math
import numpy as np


def calc_R(x,y, xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return np.sqrt((x-xc)**2 + (y-yc)**2)

def f(c, x, y):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(x, y, *c)
    return Ri - Ri.mean()

def leastsq_circle(x,y):
    # coordinates of the barycenter
    x_m = np.mean(x)
    y_m = np.mean(y)
    center_estimate = x_m, y_m
    center, ier = leastsq(f, center_estimate, args=(x,y))
    xc, yc = center
    Ri       = calc_R(x, y, *center)
    R        = Ri.mean()
    residu   = np.sum((Ri - R)**2)
    return xc, yc, R, residu



    

@app.route('/circle', methods=['post'])
def get_Circle():
    #{x:number, y: number}[]
    data = request.get_json()
    x = []
    y = []

    
    #Filter points, so point clutters dont affect the result
    smallesX = 0
    highestX = 0
    smallesY = 0
    highestY = 0

    for point in data:
        x.append(point["x"])
        y.append(point["y"])
        if point["x"] > highestX: 
            highestX = point["x"]

        if point["x"] < smallesX: 
            smallesX = point["x"]

        if point["y"] > highestY: 
            highestY = point["y"]

        if point["y"] < smallesY: 
            smallesY = point["y"]

    clusterSizeX = (highestX - smallesX) / 10
    clusterSizeY = (highestY - smallesY) / 10


    dataCopy = copy.deepcopy(data)

    for point in data:
        isStillAPoint = False
        dataCopyLength = len(dataCopy)
        pointIndex = -1

        for index, pointCopy  in enumerate(dataCopy):
            if pointCopy["x"] == point["x"] and pointCopy["y"] == point["y"]:
                pointIndex = index
                isStillAPoint = True

        if isStillAPoint:
            dataCopy = list(filter(lambda item: (item["x"] == point["x"] and item["y"] == point["y"]) or ((abs(point["x"] - item["x"]) > clusterSizeX )or (abs(point["y"] - item["y"]) > clusterSizeY)), dataCopy))

        if pointIndex > 0: 
            dataCopy[pointIndex]["weight"] = dataCopyLength -len(dataCopy)



    xc, yc, r, residue=leastsq_circle(x,y)

    meanCircleDistance = 0

    for point in dataCopy:
        distance = abs(np.sqrt((point["x"]-xc)**2 + (point["y"]-yc)**2) - r)
        point["distanceToCircle"] = distance
        meanCircleDistance = meanCircleDistance + distance

    meanCircleDistance = meanCircleDistance / len(dataCopy)

    validAnglePoints = list(filter(lambda item: item["distanceToCircle"]<meanCircleDistance , dataCopy))

    biggestAngle = 0
    anglePointB = None

    pointA  = validAnglePoints[0]
    
    for j, pointB in enumerate(validAnglePoints):
        
        vecAX = pointA["x"] - xc
        vecAY = pointA["y"] - yc
        vecBX = pointB["x"] - xc
        vecBY = pointB["y"] - yc

        
        angle = math.atan2(vecAY, vecAX) - math.atan2(vecBY,  vecBX)

        if angle < 0:
                angle += 2 * np.pi

        if angle > biggestAngle:
            biggestAngle = angle
            anglePointB = pointB




    return jsonify({
        "circle": {
            "x": xc,
            "y": yc,
            "r": r
        },
        "angle": {
            "angle": biggestAngle*(180/np.pi),
            "pointA" : pointA,
            "pointB" : anglePointB
        },
        "filteredPoints": dataCopy
    })
    
