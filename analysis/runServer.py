from flask import Flask

app = Flask(__name__)

import getAngle # Angle Calculation
import analysis # Position Data Calculation


app.run(host="0.0.0.0", port=4000, threaded=True, ssl_context='adhoc')