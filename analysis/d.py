import pandas as pd
import json
import numpy as np
with open("posit.json") as m:
    data = json.load(m)
    AX = []
    AY = []
    AZ = []
    GX = []
    GY = []
    GZ = []
    for d in data['rawData']:
        AX.append(d["acc"]["x"])
        AY.append(d["acc"]["y"])
        AZ.append(d["acc"]["z"])
        GX.append(d["gyro"]["x"])
        GY.append(d["gyro"]["y"])
        GZ.append(d["gyro"]["z"])
    dataframe = pd.DataFrame({'AX': AX,'AY': AY,'AZ': AZ, 'GX': GX,'GY': GY,'GZ': GZ})
    dataframe.to_csv("data.csv")