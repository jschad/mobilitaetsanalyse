import pandas as pd
import matplotlib.pyplot as plt
import itertools as it
import operator
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from madgwickahrs import MadgwickAHRS
from scipy.spatial.transform import Rotation as R
from scipy.signal import filtfilt, butter
from flask import Flask, escape, request, abort, jsonify
import logging
logging.basicConfig(filename='example.log', level=logging.DEBUG)
from scipy.integrate import cumtrapz
from __main__ import app

np.set_printoptions(precision=4,suppress=True)

@app.route("/analysis", methods=["POST"])
def analysis():
    data = request.get_json()
    #reformat Raw Data for Calculation
    acc = data["acc"]
    gyr = data["gyr"]
    gyrshaped = np.reshape(gyr,(int(len(gyr)/3),3))
    accshaped = np.reshape(acc,(int(len(acc)/3),3))
    length = accshaped.shape[0]
    sampleperiod = 1/50
    #use AHRS Library for Orientation
    ahrs = MadgwickAHRS(sampleperiod=sampleperiod);
    r = np.zeros((3,3,np.shape(accshaped)[0]));   
    for i,g in enumerate(gyrshaped[:length]):
        ahrs.update_imu(g *np.pi /180 , accshaped[i]);# gyroscope units must be radians
        r[:,:,i] = ahrs.quaternion.to_rotMat().T
    tcAcc = np.zeros(accshaped.shape)
    #multiply Orientation r with Acceleration Data
    for i in range(0,tcAcc.shape[0]):
       tcAcc[i] = r[:,:,i] @ accshaped[i]
    #linAcc = tcAcc - np.array([np.zeros(length), np.zeros(length), np.ones(length)]).T
    #linAcc = linAcc * 9.81
    linAcc = tcAcc
    #integrate Acceleration for Velocity
    linVel = cumtrapz(linAcc.T,dx=sampleperiod)
    #Filter with High Pass
    b,a= butter(1, 0.2/100, 'high' )
    filteredlinVel = filtfilt(b,a,linVel,axis=1,padtype = 'odd', padlen=3*(max(len(b),len(a))-1))
    """
    linPos =  np.zeros(tcAcc.shape)
    for i in range(1,length):
        linPos[i]= linPos[i-1] + filteredlinVel[i] * sampleperiod;
    """
    #integrate Velocity for Position
    linPos =cumtrapz(filteredlinVel,dx=sampleperiod) 
    #Filter it again with HighPass filter
    linPosHP = filtfilt(b,a,linPos,axis=1,padtype = 'odd', padlen=3*(max(len(b),len(a))-1))

    linDict = {
        "x" : linPosHP[0].tolist(),
        "y" : linPosHP[1].tolist(),
        "z" : linPosHP[2].tolist(),
    }
    #return Position Data
    return jsonify(linDict)