(function() {
  window.addEventListener("tizenhwkey", function(ev) {
    var activePopup = null,
      page = null,
      pageId = "";

    if (ev.keyName === "back") {
      activePopup = document.querySelector(".ui-popup-active");
      page = document.getElementsByClassName("ui-page-active")[0];
      pageId = page ? page.id : "";

      if (pageId === "main" && !activePopup) {
        try {
          tizen.application.getCurrentApplication().exit();
        } catch (ignore) {}
      } else {
        window.history.back();
      }
    }
  });
})();

document
  .getElementById("exercise-start-1")
  .addEventListener("click", () => prepareRecording("Walk", 1));
document
  .getElementById("exercise-start-2")
  .addEventListener("click", () => prepareRecording("Walk", 2));
document
  .getElementById("exercise-start-3")
  .addEventListener("click", () => prepareRecording("Walk", 3));
document
  .getElementById("exercise-start-4")
  .addEventListener("click", () => prepareRecording("Walk", 4));
document
  .getElementById("exercise-start-5")
  .addEventListener("click", () => prepareRecording("Walk", 5));

function setLabelText(labelId, text) {
  document.getElementById(labelId).innerHTML = text;
}

function hideButton(number) {
  document.getElementById("exercise-start-" + number).style.display = "none";
}

function showButton(number) {
  document.getElementById("exercise-start-" + number).style.display =
    "inline-block";
}

function prepareRecording(motion, number) {
  hideButton(number);
  navigator.vibrate([200, 800, 200, 800, 200, 800, 1500]);
  setLabelText("status-label-" + number, "Übung startet in 3");
  setTimeout(
    () => setLabelText("status-label-" + number, "Übung startet in 2"),
    1000
  );
  setTimeout(
    () => setLabelText("status-label-" + number, "Übung startet in 1"),
    2000
  );
  setTimeout(() => startRecording(motion, number), 4500);
}

function startRecording(motion, number) {
  setLabelText("status-label-" + number, "Übung wird ausgeführt...");
  const jsonData = { type: motion, data: [] };

  const accSensor = tizen.sensorservice.getDefaultSensor("LINEAR_ACCELERATION");
  const gyroSensor = tizen.sensorservice.getDefaultSensor("GYROSCOPE");

  function onGetSuccessAcc(sensorData, i) {
    jsonData.data[i].acc = {
      x: sensorData.x,
      y: sensorData.y,
      z: sensorData.z
    };
  }

  function onGetSuccessGyro(sensorData, i) {
    jsonData.data[i].gyro = {
      x: sensorData.x,
      y: sensorData.y,
      z: sensorData.z
    };
  }

  let gyroHasStarted = false;
  let accHasStarted = false;
  let counter = 0;
  let interval = 0;
  function initInterval() {
    return setInterval(function() {
      // do your thing
      const i = counter;
      jsonData.data[counter] = { timestamp: Date.now() };

      accSensor.getLinearAccelerationSensorData(sensorData =>
        onGetSuccessAcc(sensorData, i)
      );
      gyroSensor.getGyroscopeSensorData(sensorData =>
        onGetSuccessGyro(sensorData, i)
      );

      counter++;
      if (counter === 200) {
        clearInterval(interval);
        console.log(jsonData);
        navigator.vibrate(1500);
        setLabelText(
          "status-label-" + number,
          "Ergebnisse werden berechnet..."
        );
        sendData(jsonData);
      }
    }, 20);
  }

  gyroSensor.start(() => {
    gyroHasStarted = true;
    if (accHasStarted) interval = initInterval();
  });
  accSensor.start(() => {
    accHasStarted = true;
    if (gyroHasStarted) interval = initInterval();
  });

  function sendData(json) {
    var myRequest = new XMLHttpRequest();
    myRequest.open("POST", "https://watch.jschad.de/movement/create", true);
    myRequest.setRequestHeader(
      "Content-Type",
      "application/json;charset=UTF-8"
    );
    myRequest.onload = () => {
      // print JSON response
      if (myRequest.status >= 200 && myRequest.status < 300) {
        // parse JSON
        const response = JSON.parse(myRequest.responseText);
        console.log(response);
        showButton(number);
        setLabelText("status-label-" + number, "Das Ergebnis lautet xx.");
      }
    };
    myRequest.send(JSON.stringify(json));
  }

  /*
  fetch("http://192.168.178.30:3002/movement/create", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ type: "Walk" }),
  })
    .then((res) => res.json())
    .then((json) => console.log(json));
    



  const payload = JSON.stringify({ type: "Walk" });
  console.log(new Blob([payload]).size);
  var myRequest = new XMLHttpRequest();
  myRequest.open("POST", "http://192.168.178.30:3002/movement/create", true);
  myRequest.setRequestHeader("Content-Type", "application/json");
  myRequest.setRequestHeader('Header-Custom-TizenCORS', 'OK');
  myRequest.setRequestHeader(
    "Content-Length",
    new Blob([payload]).size
    //encodeURI(payload).split(/%..|./).length - 1
  );
  myRequest.onload = function (data) {
    console.log(data);
  };
  myRequest.send(payload);



  var myRequest = new XMLHttpRequest();
  myRequest.open("GET", "https://watch.jschad.de/movement/test", true);
  // myRequest.setRequestHeader("Header-Custom-TizenCORS", "OK");
  myRequest.send();

  */
}
